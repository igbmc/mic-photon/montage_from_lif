// We need bio-formats to open lif files
run("Bio-Formats Macro Extensions");

// ****************	 USER-DEFINED VARIABLES **************************
// Please select the channel to use for the montage
channelToUse = 2;
numberColumns = 6;


// Ask user to define an input folder
imgDir = getDirectory("Source Directory with Images");
// Ask user to define an output folder
outputDir = getDirectory("Destination Directory for Output");
// Get list of files contained in the Input folder
fileList = getFileList(imgDir);


// THIS LOOP OPENS ALL IMAGES FOR SELECTED CHANNEL OF ALL SERIES OF ALL LIF FILES IN INPUT FOLDER
// Go through all the files..
for (i = 0; i < fileList.length; i++) {
	// .. only if they are LIF files
	if (endsWith(fileList[i], ".lif")) {
		filePath = imgDir + fileList[i];
		// select a LIF file
		Ext.setId(filePath);
		// check how many series it contains
		Ext.getSeriesCount(nSeries);
		// loop through all the series
		for(j = 0; j < nSeries; j++) {
			// select the current series
			Ext.setSeries(j);
			// and get its name
			Ext.getSeriesName(seriesName);
			// actually open the selected series
			run("Bio-Formats Importer", "open=[" + filePath + "] color_mode=Composite rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_list=" + j + 1);
			imgName = getTitle();
			// extract the wanted channel by duplicating it
			run("Duplicate...", "duplicate channels=" + channelToUse);
			// close original image
			close(imgName);
			rename(imgName);
			// Set auto contrast for the opened image
		    run("Enhance Contrast", "saturated=0.35"); // that's 'Auto' pressed once
		    // Apply it to the image to actually change it
    		run("Apply LUT", "slice");
    		// Convert it to RGB image so we can have white annotations on top
    		run("RGB Color");
    		// resize image otherwise the montage will be huge
			run("Size...", "width=512 height=512 depth=1 constrain average interpolation=None");
			// Place some annotations on the montage
			getDimensions(width, height, channels, slices, frames); // we need to know how large the image is
			setFont("SansSerif", 20); // change the font and size of text if you want
			stringWidth = getStringWidth(seriesName); // and how long the string for the annotation is
			// actuall draw the annotation with the series name
			drawString(seriesName, width-stringWidth-10, 30);
			rename(seriesName);
		}
	}
}

// Now that all images are open, make a list of all opened images
imgs = getList("image.titles");
// Sort them alphabetically
Array.sort(imgs);

// We want to concatenate them in order so we have to manually build the command string
concatenateString = "open ";
for (k = 0; k < imgs.length; k++) {
	concatenateString += " image"+k+1 + "=["+imgs[k]+"]"; // concatenate imageN with name of image
}

// Concatenate all images in alphabetical order
run("Concatenate...", concatenateString + " title=Stack");
// Out of that stack make a montage
run("Make Montage...", "columns=" + numberColumns + " rows=" + -floor(-(imgs.length/numberColumns)) + " scale=1");
// Embed the colorscale into the image
run("Flatten");
// Save the image to disk
fileName = "allseries-channel" + channelToUse + "-autoContrast-montage.png";
saveAs("PNG", outputDir + fileName);
// close images
close("Montage");
close("Stack");
close(fileName);
