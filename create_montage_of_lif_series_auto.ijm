// We need bio-formats to open lif files
run("Bio-Formats Macro Extensions");

// ****************	 USER-DEFINED VARIABLES **************************
// Please define min and max values for each channel
minValue1 = 500;
maxValue1 = 30000;
minValue2 = 500;
maxValue2 = 20000;
minValue3 = 500;
maxValue3 = 20000;


// Ask user to define an input folder
imgDir = getDirectory("Source Directory with Images");
// Ask user to define an output folder
outputDir = getDirectory("Destination Directory for Output");
// Get list of files contained in the Input folder
fileList = getFileList(imgDir);
// Go through all the files..
for (i = 0; i < fileList.length; i++) {
	// .. only if they are LIF files
	if (endsWith(fileList[i], ".lif")) {
		filePath = imgDir + fileList[i];
		// select a LIF file
		Ext.setId(filePath);
		// check how many series it contains
		Ext.getSeriesCount(nSeries);
		// loop through all the series
		for(j = 0; j < nSeries; j++) {
			// select the current series
			Ext.setSeries(j);
			// and get its name
			Ext.getSeriesName(seriesName);
			// actually open this series only
			run("Bio-Formats Importer", "open=[" + filePath + "] color_mode=Composite rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_list=" + j + 1);
			// For each channel set min and max contrast
			Stack.setChannel(1);
			// Set auto contrast for the opened image
		    run("Enhance Contrast", "saturated=0.35"); // that's 'Auto' pressed once
		    // Apply it to the image to actually change it
    		run("Apply LUT", "slice");
			Stack.setChannel(2);
			// Set auto contrast for the opened image
		    run("Enhance Contrast", "saturated=0.35"); // that's 'Auto' pressed once
		    // Apply it to the image to actually change it
    		run("Apply LUT", "slice");			
			Stack.setChannel(3);
			// Set auto contrast for the opened image
		    run("Enhance Contrast", "saturated=0.35"); // that's 'Auto' pressed once
		    // Apply it to the image to actually change it
    		run("Apply LUT", "slice");			


			// Remember image name
			imgName = getTitle();
			// Convert from a multi-channel image to a horizontal montage
			run("Make Montage...", "columns=3 rows=1");
			rename(seriesName + "-montage");
			// Place some annotations on the montage
			getDimensions(width, height, channels, slices, frames); // we need to know how large the image is
			stringWidth = getStringWidth(seriesName); // and how long the string for the annotation is
			// Draw the name of the series on the top right
			drawString(seriesName, width-stringWidth-10, 20);
			// Save image to disk
			saveAs("PNG", outputDir + imgName + "-montage.png");
			// Close image
			close(imgName);
			close(imgName + "-montage.png");
		}
	}
}
