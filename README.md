# Create image montage for series inside LIF files
## Description
This repository contains several utiliy ImageJ macros for creating montage from LIF files. The idea is to be able to compare many different multichannel images contained in LIF files with each other.

## Macro descriptions
### `create_montage_of_lif_series.ijm`
This script creates a montage for each series contained in all LIF files in the input folder. The montage is export to disk for every series, where each channel contrast can be adjusted independently. The series name and min and max values used for the contrast are written in the image.

### `montage_onechannel_all_series.ijm`
This script creates a montage of a selected channel with user-defined min and max contrast settings for every LIF file in the Input file.

### `montage_onechannel_all_series_together.ijm`
This script does the same operation as the one above, but it considers all LIF files in the Input folder at the same time and will create a large montage with all series contained in all LIF files sorted alphabetically. Also with fixed contrast settings. Please use this script if comparing the intensity of the signal in the montage is important.

### `montage_onechannel_all_series_together_auto.ijm`
This script does the same operation as the one above, but it treats every image as independent for setting the contrast settings. Please use this script if comparing the stained structure is more important than the amount of signal in the montage.

## Data structure
- Create a folder containing all the .lif files you want to process together. You could split your files in different folders and run the script multiple times on each folder or you could group all the data in the same folder depending on how you want your final montage to be.
- Create a a folder that will contain the _output_ of the script, i.e. the montage(s).

A typical folder structure could be represented by the tree below. Here the Data would contain the .lif files, the Macros folder would contain the scripts of this repository and the Montages folder would contain the output of the scripts.
```
./project/
├── Data
├── Macros
└── Montages
```

## Installation
- Install Fiji from [here](https://imagej.net/software/fiji/downloads) and launch it
- Open a macro file by drag and dropping into the main Fiji window
- Press Run the follow the prompts to select Input and Output folders

## Author
Marco Dalla Vecchia dallavem@igbmc.fr
